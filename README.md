# PeerTube plugin Quickstart with Vite

This quickstart includes [Vite](https://vitejs.dev/) bunbler configuration based on [Rollup](https://rollupjs.org).

See basics on https://docs.joinpeertube.org/#/contribute-plugins?id=write-a-plugintheme

## Lazy-loading

Sometimes we need to lazy-load a library with ES `import('my-library').then(m => mo.default)`, so chunks will be created by Rollup.

To rename the generated chunks your need to update `vite.config.js`:

```javascript
export default {
  build: {
    ...,
    rollupOptions: {
      output: {
        chunkFileNames: '[name].js'
      }
    }
  }
}
```
This way they will be paced in `dist/my-library.js`.


To serve them with PeerTube you need to update `package.json`:

```json
{
  ...,
  "clientScripts": [
    ...,
    {
      "script": "dist/my-library.js",
      "scopes": [] // do not load it in any scope
    }
  ]
}
```

## Build multiple client files

Vite cannot build multiple libraries at once.

The most common way is to create a specific config for each scope: `vite.config.common.js`, `vite.config.signup.js`,...

You also need to update `package.json`:

```json
{
  ...,
  "scripts": {
    "build": "vite --config vite.config.common.js build; vite --config vite.config.signup.js build; ..."
  }
}
```

## Live-reload testing

Here is a way to watch every **client-side plugin changes in the runtime code** and live-reload on http://localhost.

Note that for any **server-side and global CSS plugin changes** the only way to see these changes is to:

- Stop PeerTube
- Re-install the plugin
- Re-run PeerTube

### In your PeerTube directory

```bash
$ nvm use 12                    # Make sure you're using Node.js 12
$ yarn install --pure-lockfile  # Install PeerTube dependencies
$ npm run build -- --light      # Build PeerTube
$ npm run plugin:install -- \
  --plugin-path /path/to/plugin # Install plugin
$ npm start                     # Run PeerTube on http://localhost:9000
```

### In your plugin directory

Install `rollup-plugin-livereload`:

```bash
$ npm i -D rollup-plugin-livereload
```

Update `vite.config.js`:

```javascript
import { resolve } from 'path'
import { loadEnv } from 'vite'
import pkg from './package.json'
import livereload from 'rollup-plugin-livereload'

// npx vite build -m development - build and watch without minifying in PeerTube environment
// npx vite build -m staging - build without minifying
// npx vite build [-m production] - build and minify

export default ({ mode }) => {
  const env = loadEnv(mode, process.cwd(), 'PEERTUBE_')
  const buildPeerTubeDistPath = () => env.PEERTUBE_PATH && resolve(env.PEERTUBE_PATH, `./storage/plugins/node_modules/${pkg.name}/`)

  return {
    build: {
      outDir: resolve(buildPeerTubeDistPath() || './', 'dist'),
      watch: mode === 'development',
      minify: mode === 'production' && 'terser',
      lib: {
        entry: 'client/common-client-plugin.js',
        name: pkg.name,
        formats: ['es'],
        fileName: () => 'common-client-plugin.js'
      }
    },
    plugins: [ mode === 'development' && livereload() ]
  }
}
```

You need to create `.env.development.local` file and define:

```dosini
PEERTUBE_PATH=/path/to/local/PeerTube
```
