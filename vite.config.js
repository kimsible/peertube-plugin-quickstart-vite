export default {
  build: {
    lib: {
      entry: 'client/common-client-plugin.js',
      name: 'my-plugin',
      formats: ['es'],
      fileName: () => 'common-client-plugin.js'
    }
  }
}
